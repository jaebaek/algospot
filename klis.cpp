#include <iostream>
#include <vector>
#include <assert.h>

const int MAX = 500;
const int BASE = 1 << 30;

using namespace std;

void init(std::vector<int>& a, int b)
{
    int h = b / BASE;
    int l = b % BASE;
    a.push_back(l);
    if (h) {
        a.push_back(h);
    }
}

void add(std::vector<int>& a, const std::vector<int>& b)
{
    if (!b.size()) return;
    if (b.size() == 1 && !b[0]) return;

    int carry = 0;
    int i = 0;
    int next = 0;
    while (i < a.size() && i < b.size()) {
        a[i] += b[i] + carry;
        carry = a[i] / BASE;
        a[i] = a[i] % BASE;
        ++i;
    }
    while (i < a.size() && carry) {
        a[i] += carry;
        carry = a[i] / BASE;
        a[i] = a[i] % BASE;
        ++i;
    }
    while (i < b.size()) {
        next = b[i] + carry;
        carry = next / BASE;
        next = next % BASE;
        a.push_back(next);
        ++i;
    }
    if (carry) {
        a.push_back(carry);
    }
}

// A must be larger than B
void rmv(std::vector<int>& a, const std::vector<int>& b)
{
    int carry = 0;
    int i = 0;
    while (i < b.size()) {
        a[i] -= b[i] + carry;
        if (a[i] < 0) {
            carry = 1;
            a[i] += BASE;
        } else {
            carry = 0;
        }
        ++i;
    }
    while (i < a.size() && carry) {
        a[i] -= carry;
        if (a[i] < 0) {
            carry = 1;
            a[i] += BASE;
        } else {
            carry = 0;
            break;
        }
        ++i;
    }
    assert(!carry);
}

// return if a > b
int cmp(const std::vector<int>& a, const std::vector<int>& b)
{
    int nza = 0;
    int nzb = 0;
    int i = 0;

    for (i = a.size()-1; i >= 0; --i)
        if (a[i])
            break;
    nza = i+1;

    for (i = b.size()-1; i >= 0; --i)
        if (b[i])
            break;
    nzb = i+1;

    if (nza > nzb)
        return 1;
    if (nza < nzb)
        return 0;
    for (int i = nza-1; i >= 0; --i) {
        if (a[i] > b[i])
            return 1;
        if (a[i] < b[i])
            return 0;
    }
    // they are same
    return 0;
}

void process()
{
    // input
    int n, k;
    int input[MAX];
    cin >> n >> k;
    if (!n || !k) assert(0);
    for (int i = 0; i < n; ++i) {
        cin >> input[i];
    }

    // get increasing sequence and length of longest iseq
    int iseq[MAX];
    int lis = 0;
    for (int i = 0; i < n; ++i) {
        iseq[i] = 0;
        for (int j = 0; j < i; ++j) {
            if (input[j] < input[i] && iseq[j] > iseq[i]) {
                iseq[i] = iseq[j];
            }
        }
        ++iseq[i];
        if (iseq[i] > lis) {
            lis = iseq[i];
        }
    }
    cout << lis << endl;

    // make a tree with # of its children
    std::vector<int> cnt[MAX];
    for (int i = 0; i < n; ++i) {
        if (iseq[i] == lis) {
            cnt[i].push_back(1);
        } else {
            cnt[i].push_back(0);
        }
    }
    for (int len = lis-1; len > 0; --len) {
        for (int i = 0; i < n; ++i) {
            if (iseq[i] == len) {
                for (int j = i+1; j < n; ++j) {
                    if (iseq[j] == len+1 && input[i] < input[j]) {
                        add(cnt[i], cnt[j]);
                    }
                }
            }
        }
    }

    // get total # of lis
    std::vector<int> total;
    for (int i = 0; i < n; ++i) {
        if (iseq[i] == 1) {
            add(total, cnt[i]);
        }
    }

    // find K th lis
    std::vector<int> kv;
    init(kv, k);
    std::vector<int> next;
    int len = 1;
    int last = 0;
    for (int i = 0; i < n; ++i) {
        if (iseq[i] == len && last < input[i]) {
            next = total;
            rmv(next, cnt[i]);
            if (cmp(kv, next)) {
                last = input[i];
                cout << last << " ";
                ++len;
            } else {
                total = next;
            }
        }
    }
    cout << endl;
}

int main(int argc, const char *argv[])
{
    int c = 0;
    cin >> c;
    while (c--) {
        process();
    }
    return 0;
}

#include <stdio.h>

#define MAX 500

int n;
int seq[MAX];
int lis[MAX];

int run()
{
    /* get longest increase sequence */
    lis[0] = 1;
    for (int i = 1; i < n; ++i) {
        lis[i] = 1;
        for (int j = 0; j < i; ++j) {
            if (seq[i] > seq[j] && lis[j]+1 > lis[i]) {
                lis[i] = lis[j]+1;
            }
        }
    }

    int max = 0;
    for (int i = 0; i < n; ++i) {
        if (max < lis[i]) {
            max = lis[i];
        }
    }
    return max;
}

int main(int argc, const char *argv[])
{
    int c;
    scanf("%d", &c);
    while (c--) {
        /* input */
        scanf("%d", &n);
        for (int i = 0; i < n; ++i) {
            scanf("%d", &seq[i]);
        }

        /* init */
        for (int i = 0; i < n; ++i) {
            lis[i] = 0;
        }

        if (n) {
            int len = run();
            printf("%d\n", len);
        }
    }
    return 0;
}

#include <iostream>
#include <vector>

using namespace std;

int c, n, m;

int included(std::vector<int>& v, int x)
{
    for (int i = 0; i < v.size(); ++i) {
        if (v[i] == x) {
            return 1;
        }
    }
    return 0;
}

int couple_set(std::vector<int>& couples1, std::vector<int>& couples2, std::vector<int>& picked, int j)
{
    int ans = 0;
    if (picked.size() == n) {
        return 1;
    }
    for (int i = j; i < couples1.size(); ++i) {
        if (included(picked, couples1[i]) || included(picked, couples2[i])) {
            continue;
        }
        // if not picked
        picked.push_back(couples1[i]);
        picked.push_back(couples2[i]);
        ans += couple_set(couples1, couples2, picked, i + 1);
        picked.pop_back();
        picked.pop_back();
    }
    return ans;
}

int main(int argc, const char *argv[])
{
    cin >> c;
    while (c--) {
        std::vector<int> couples1;
        std::vector<int> couples2;
        std::vector<int> picked;
        cin >> n >> m;
        for (int i = 0; i < m; ++i) {
            int a, b;
            cin >> a >> b;
            couples1.push_back(a);
            couples2.push_back(b);
        }
        int ans = couple_set(couples1, couples2, picked, 0);
        cout << ans << endl;
    }
    return 0;
}

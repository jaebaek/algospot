#include <iostream>
#include <vector>
#include <assert.h>

using namespace std;

int cent(const std::vector<int>& v, const int& beg, const int& end)
{
    int i = (beg+end)/2;
    int j = i+1;
    int ht = min(v[i], v[j]);
    int ret = 2*ht;

    while (beg <= i && j <= end) {
        while (beg <= (i-1) && ht <= v[(i-1)]) --i;
        while (end >= (j+1) && ht <= v[(j+1)]) ++j;

        int sz = (j-i+1) * ht;
        ret = max(sz, ret);

        int lht = 0;
        if (beg <= (i-1))
            lht = v[(i-1)];
        int rht = 0;
        if (end >= (j+1))
            rht = v[(j+1)];
        if (lht > rht) {
            ht = lht;
            --i;
        } else {
            ht = rht;
            ++j;
        }
    }
    return ret;
}

int rect(const std::vector<int>& v, const int& beg, const int& end)
{
    if (beg >= end) {
        return v[beg];
    }
    const int mid = (beg+end)/2;
    const int maxl = rect(v, beg, mid);
    const int maxr = rect(v, mid+1, end);
    const int maxc = cent(v, beg, end);
    return max(maxl, max(maxr, maxc));
}

void process()
{
    int n = 0;
    std::vector<int> input;
    cin >> n;
    assert(n);
    for (int i = 0; i < n; ++i) {
        int tmp = 0;
        cin >> tmp;
        input.push_back(tmp);
    }
    cout << rect(input, 0, n-1) << endl;
}

int main(int argc, const char *argv[])
{
    int c = 0;
    cin >> c;
    while (c--) {
        process();
    }
    return 0;
}

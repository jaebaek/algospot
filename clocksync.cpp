#include <iostream>

#define CLOCKS 16
#define SWCHES 10
#define SWCHES_SIZE 5
#define MAX 31

using namespace std;

int clock_time[CLOCKS];
int swch[SWCHES][SWCHES_SIZE] = {
    {0, 1, 2}, {3, 7, 9, 11},
    {4, 10, 14, 15}, {0, 4, 5, 6, 7},
    {6, 7, 8, 10, 12}, {0, 2, 14, 15},
    {3, 14, 15}, {4, 5, 7, 14, 15},
    {1, 2, 3, 4, 5}, {3, 4, 5, 9, 13}
};
int swch_size[SWCHES] = {3, 4, 4, 5, 5, 4, 3, 5, 5, 5};

// check if all clocks show 12 o'clock
bool done()
{
    for (int i = 0; i < CLOCKS; ++i) {
        if ((clock_time[i] % 12) != 0) {
            return false;
        }
    }
    return true;
}

int press(int next)
{
    if (done()) return 0;

    // if already tried all switches and still not done,
    // it is impossible (-1)
    if (next == SWCHES) return -1;

    int ans = MAX;
    for (int i = 0; i < 4; ++i) {
        int delta = 3*i;
        for (int j = 0; j < swch_size[next]; ++j) {
            clock_time[swch[next][j]] += delta;
        }
        int cand = press(next+1);
        for (int j = 0; j < swch_size[next]; ++j) {
            clock_time[swch[next][j]] -= delta;
        }
        if (cand != -1) {
            ans = min(cand+i, ans);
        }
    }
    return ans == MAX ? -1 : ans;
}

int main(int argc, const char *argv[])
{
    int c;
    cin >> c;
    while (c--) {
        for (int i = 0; i < CLOCKS; ++i) {
            cin >> clock_time[i];
        }
        cout << press(0) << endl;
    }
    return 0;
}

#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

#define MAX 1001
#define X 'x'
#define W 'w'
#define B 'b'

char input[MAX] = {0};
int input_size = 0;
char tmp[MAX] = {0};

int inverse(int ofs)
{
    if (input[ofs] != X) {
        return 1;
    }

    int length = 1;
    int sublength[4] = {0};
    for (int i = 0; i < 4; ++i) {
        int cur_ofs = ofs + length;
        sublength[i] = inverse(cur_ofs);
        length += sublength[i];
    }

    sublength[0] = sublength[0] + sublength[1];
    sublength[1] = sublength[2] + sublength[3];

    int start = length + ofs;
    int k = ofs + 1;
    for (int i = 0; i < 2; ++i) {
        start -= sublength[i];
        for (int j = start; j < start+sublength[i]; ++j) {
            tmp[j] = input[k];
            ++k;
        }
    }
    for (int i = ofs + 1; i < ofs + length; ++i) {
        input[i] = tmp[i];
    }
    return length;
}

int main(int argc, const char *argv[])
{
    int c;
    cin >> c;
    while (c--) {
        scanf("%s", input);
        input_size = strlen(input);
        inverse(0);
        printf("%s\n", input);
    }
    return 0;
}

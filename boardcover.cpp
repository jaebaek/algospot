#include <iostream>
#include <stdbool.h>

#define HSIZE 20
#define WSIZE 20

using namespace std;

int h, w;
bool board[HSIZE][WSIZE]; // true == not covered, false == covered

int count()
{
    int cnt = 0;
    for (int i = 0; i < h; ++i) {
        for (int j = 0; j < w; ++j) {
            if (board[i][j]) {
                ++cnt;
            }
        }
    }
    return cnt;
}

void dump()
{
    for (int i = 0; i < h; ++i) {
        for (int j = 0; j < w; ++j) {
            if (board[i][j]) {
                cout << ".";
            } else {
                cout << "#";
            }
        }
        cout << endl;
    }
}

int cover()
{
    if (count() % 3 != 0) return 0;

    int ans = 0;
    int i, j;
    for (i = 0; i < h; ++i) {
        bool end = false;
        for (j = 0; j < w; ++j) {
            if (board[i][j]) {
                end = true;
                break;
            }
        }
        if (end) {
            break;
        }
    }
    if (i == h) {
        return 1;
    }
    board[i][j] = false;
    if (j < w-1 && board[i][j+1] && i < h-1 && board[i+1][j]) {
        board[i][j+1] = false;
        board[i+1][j] = false;
        ans += cover();
        board[i][j+1] = true;
        board[i+1][j] = true;
    }
    if (j < w-1 && board[i][j+1] && i < h-1 && board[i+1][j+1]) {
        board[i][j+1] = false;
        board[i+1][j+1] = false;
        ans += cover();
        board[i][j+1] = true;
        board[i+1][j+1] = true;
    }
    if (j > 0 && board[i+1][j-1] && i < h-1 && board[i+1][j]) {
        board[i+1][j-1] = false;
        board[i+1][j] = false;
        ans += cover();
        board[i+1][j-1] = true;
        board[i+1][j] = true;
    }
    if (j < w-1 && board[i+1][j+1] && i < h-1 && board[i+1][j]) {
        board[i+1][j+1] = false;
        board[i+1][j] = false;
        ans += cover();
        board[i+1][j+1] = true;
        board[i+1][j] = true;
    }
    board[i][j] = true;
    return ans;
}

int main(int argc, const char *argv[])
{
    int c;
    cin >> c;
    while (c--) {
        char buf[WSIZE] = {0};
        cin >> h >> w;
        for (int i = 0; i < h; ++i) {
            cin >> buf;
            for (int j = 0; j < w; ++j) {
                board[i][j] = (buf[j] == '.');
            }
        }
        cout << cover() << endl;
    }
    return 0;
}
